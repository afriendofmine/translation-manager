Translation Manager
===================
The Translation Manager can be used for importing and exporting language labels to PHP and JSON files. The package currently supports only CSV file's from external sources like Google.

![IMAGE](https://phpci.campaignapps.nl/build-status/image/6)

Install the package
-------------------
Installation is a quick 2 step process:

1. Download translation-manager using composer
2. Configure the config file

### Step 1: Download translation-manager  using composer

```bash
composer require afom/translation-manager
```

### Step 2: Configure the config file

Copy the config.example.php in the config folder to config.php and setup the external URL. 


Usage
-----

### Example 1: Plain PHP

``` php
$importer = new ExternalCsvImporter(new Client(), new CsvParser());
$writer = new PHPWriter(new Filesystem(), new ArrayMapper(), '/path/to/export');

$translations = $importer->import('https://docs.google.com/awesome-sheet.csv');
$writer->write($translations, 'app');
```

### Example 2: Use Console Command

```bash
php application import:labels
```

Any questions?
--------------
You can reach us at **development@afriendofmine.nl**
