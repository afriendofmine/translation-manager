<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Command;

use Afom\TranslationManager\Exception\ConfigException;
use Afom\TranslationManager\Importer\ExternalCsvImporter;
use Afom\TranslationManager\Importer\ImporterInterface;
use Afom\TranslationManager\Importer\Parser\CsvParser;
use Afom\TranslationManager\Writer\JSONWriter;
use Afom\TranslationManager\Writer\Mapper\ArrayMapper;
use Afom\TranslationManager\Writer\PHPWriter;
use Afom\TranslationManager\Writer\WriterInterface;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ImportLabelCommand extends Command
{
    /** @var string */
    private $configPath;

    /**
     * @param string $configPath
     */
    public function __construct($configPath)
    {
        parent::__construct();

        $this->configPath = $configPath;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('import:labels')->setDescription('Import labels');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filesystem = new Filesystem();

        if ($filesystem->exists($this->configPath) === false) {
            throw new ConfigException('Config file not found');
        }

        $config = include($this->configPath);

        $importer = $this->getImporter($config);
        $writers = $this->getWriters($config);

        foreach ($this->getSourceOptions($config) as $sourceOption) {
            $translations = $importer->import($sourceOption['source']);

            foreach ($writers as $writer) {
                $writer->write($translations, $sourceOption['filename']);
            }
        }
    }

    /**
     * @param array $config
     *
     * @return array
     *
     * @throws ConfigException
     */
    private function getSourceOptions(array $config)
    {
        if ( ! isset($config['importers']['adapter']) || $config['importers']['adapter'] === null) {
            throw new ConfigException('No import adapters defined');
        }

        switch ($config['importers']['adapter']) {
            case 'external':
                return $config['importers']['external'];
            default:
                throw new ConfigException('No sources found');
        }
    }

    /**
     * @param array $config
     *
     * @return ImporterInterface
     *
     * @throws ConfigException
     */
    private function getImporter(array $config)
    {
        if ( ! isset($config['importers']['adapter']) || $config['importers']['adapter'] === null) {
            throw new ConfigException('No import adapters defined');
        }

        if ($config['importers']['adapter'] === 'external') {

            $reader = ReaderFactory::create(Type::CSV);

            return new ExternalCsvImporter(new Client(), new CsvParser($reader, new Filesystem()));
        }

        throw new ConfigException('No import adapters defined');
    }

    /**
     * @param array $config
     *
     * @return WriterInterface[]
     *
     * @throws ConfigException
     */
    private function getWriters(array $config)
    {
        if ( ! isset($config['writers']) || ! is_array($config['writers'])) {
            throw new ConfigException('No import adapters defined');
        }

        $writers = [];

        $filesystem = new Filesystem();


        foreach ($config['writers'] as $key => $configWriter) {
            if ($key === 'json') {
                $arrayMapper = new ArrayMapper();
                $arrayMapper->setStructure($configWriter['structure']);

                $writers[] = new JSONWriter($filesystem, $arrayMapper, $configWriter['destination'], $config['general']['filename']);
            }

            if ($key === 'php') {
                $arrayMapper = new ArrayMapper();
                $arrayMapper->setStructure($configWriter['structure']);

                $writers[] = new PHPWriter($filesystem, $arrayMapper, $configWriter['destination'], $config['general']['filename']);
            }
        }

        return $writers;
    }
}
