<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Importer;

use Afom\TranslationManager\Exception\ImporterException;
use Afom\TranslationManager\Importer\Parser\CsvParser;
use GuzzleHttp\ClientInterface;

class ExternalCsvImporter implements ImporterInterface
{
    /** @var ClientInterface */
    private $client;

    /** @var CsvParser */
    private $parser;

    /**
     * @param ClientInterface $client
     * @param CsvParser       $parser
     */
    public function __construct(ClientInterface $client, CsvParser $parser)
    {
        $this->client    = $client;
        $this->parser    = $parser;
    }

    /**
     * {@inheritdoc}
     */
    public function import($url)
    {
        $file = $this->createTemporaryFile();

        try {
            $response = $this->client->request('GET', $url, ['sink' => $file]);

            if ($response->getStatusCode() !== 200) {
                throw new ImporterException('The import URL is not set or defined');
            }

            // checking if the content is a CSV.
            if ($response->getHeader('Content-Type')[0] !== 'text/csv') {
                throw new ImporterException('Invalid CSV.');
            }

            return $this->parser->parse($file);
        } catch (\Exception $exception) {
            throw new ImporterException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @return string
     */
    private function createTemporaryFile()
    {
        return stream_get_meta_data(tmpfile())['uri'];
    }
}
