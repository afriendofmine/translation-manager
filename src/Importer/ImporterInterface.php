<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Importer;

use Afom\TranslationManager\Exception\ImporterException;
use Afom\TranslationManager\Model\Translation;

interface ImporterInterface
{
    /**
     * @param string $source
     *
     * @return Translation[]
     *
     * @throws ImporterException
     */
    public function import($source);
}
