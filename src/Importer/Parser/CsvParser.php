<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Importer\Parser;

use Afom\TranslationManager\Model\Translation;
use Box\Spout\Reader\ReaderInterface;
use Box\Spout\Reader\CSV\Sheet;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;

class CsvParser
{
    /** @var ReaderInterface */
    private $reader;

    /** @var Filesystem */
    private $filesystem;

    /**
     * @param ReaderInterface $reader
     * @param Filesystem      $filesystem
     */
    public function __construct(ReaderInterface $reader, Filesystem $filesystem)
    {
        $this->reader     = $reader;
        $this->filesystem = $filesystem;
    }

    /**
     * @param string $path
     *
     * @return Translation[]
     *
     * @throws FileNotFoundException
     */
    public function parse($path)
    {
        if ( ! $this->filesystem->exists($path)) {
            throw new FileNotFoundException($path);
        }

        $translations = [];

        // preparing to read the csv.
        $this->reader->open($path);

        foreach ($this->reader->getSheetIterator() as $sheet) {
            $sheetTranslations = $this->getTranslations($sheet);
            $translations = array_merge($translations, $sheetTranslations);
        }

        // closing the reader because we're done.
        $this->reader->close();

        return $translations;
    }

    /**
     * @param Sheet $sheet
     *
     * @return Translation[]
     */
    private function getTranslations(Sheet $sheet)
    {
        $locales = [];
        $translations = [];

        foreach ($sheet->getRowIterator() as $rowKey => $row) {
            // extracting the locales from the first row.
            if ($rowKey === 1) {
                $locales = $this->getLocales($row);
                continue;
            }

            // extracting the translations for the locales.
            foreach ($locales as $key => $locale) {
                $translation = new Translation($row[0], $locale, $row[$key]);

                $translations[] = $translation;
            }
        }

        return $translations;
    }

    /**
     * @param array $row
     *
     * @return array
     */
    private function getLocales(array $row)
    {
        $locales = [];

        foreach ($row as $key => $value) {
            // skipping the first column or comments column.
            if ($key === 0 || $value === 'comments') {
                continue;
            }

            // add locale while maintaining the key.
            $locales[$key] = $value;
        }

        return $locales;
    }
}
