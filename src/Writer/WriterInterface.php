<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Writer;

use Afom\TranslationManager\Model\Translation;

interface WriterInterface
{
    /**
     * @param Translation[] $translations
     * @param string        $filename
     *
     * @return void
     */
    public function write(array $translations, $filename);
}
