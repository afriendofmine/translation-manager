<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Writer;

class PHPWriter extends TranslationWriter implements WriterInterface
{
    /**
     * {@inheritdoc}
     */
    public function write(array $translations, $filename)
    {
        $ordered = $this->orderByLocale($translations);

        foreach ($ordered as $locale => $translations) {
            $structuredTranslations = $this->arrayMapper->toArray($translations);

            $destination = sprintf('%s/%s/%s.php', $this->destination, $locale, $filename);
            $content = sprintf('<?php ' . "\n \n" . 'return %s;', var_export($structuredTranslations, true));

            $this->filesystem->dumpFile($destination, $content);
        }
    }
}
