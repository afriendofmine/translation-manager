<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Writer\Mapper;

use Afom\TranslationManager\Model\Translation;
use Afom\TranslationManager\Enum\StructureType;

class ArrayMapper
{
    private $structure = StructureType::FLATTENED;

    /**
     * @param string $structure
     *
     * @throws \Exception
     */
    public function __construct($structure = '')
    {
        if ($structure !== '') {
            $this->setStructure($structure);
        }
    }

    /**
     * @param string $structure
     *
     * @throws \Exception
     */
    public function setStructure($structure)
    {
        if (in_array($structure, (new \ReflectionClass(StructureType::class))->getConstants()) === false) {
            throw new \Exception('Invalid structure given.');
        }

        $this->structure = $structure;
    }

    /**
     * @param Translation[] $translations
     *
     * @return array
     */
    public function toArray(array $translations)
    {
        $structured = [];

        foreach ($translations as $translation) {
            switch ($this->structure) {
                case StructureType::FLATTENED:
                    $this->flattened($structured, $translation);
                    break;
                case StructureType::NESTED:
                    $this->nested($structured, $translation->getIdentifier(), $translation->getContent());
                    break;
            }
        }

        return $structured;
    }

    /**
     * @param array       $array
     * @param Translation $translation
     */
    private function flattened(array &$array, Translation $translation)
    {
        $content = $translation->getContent();
        $identifierArray = explode('.', $translation->getIdentifier());

        if ($identifierArray[count($identifierArray) - 1] === 'array') {
            $content = $this->contentToArray($content);
        }

        $array[$translation->getIdentifier()] = $content;
    }

    /**
     * Borrowed function from Laravel's array_set
     * Modified for treating "array" identifier as array
     *
     * @param array  $array
     * @param string $key
     * @param string $value
     *
     * @return array
     */
    private function nested(&$array, $key, $value)
    {
        if (is_null($key)) {
            return $array;
        }

        $keys = explode('.', $key);

        while (count($keys) > 1) {
            $key = array_shift($keys);

            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if (!isset($array[$key]) || !is_array($array[$key])) {
                $array[$key] = [];
            }

            $array = &$array[$key];
        }

        $lastKey = array_shift($keys);

        // We're treating the value as an array if the key is "array" by
        // converting comma separated string into array
        // and removing white spaces on the left side.
        if ($lastKey === 'array') {
            $value = $this->contentToArray($value);
        }

        $array[$lastKey] = $value;

        return $array;
    }

    /**
     * @param string $content
     *
     * @return array
     */
    private function contentToArray($content)
    {
        return array_map(function ($string) {
            return ltrim($string);
        }, explode(',', $content));
    }
}
