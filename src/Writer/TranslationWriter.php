<?php

namespace Afom\TranslationManager\Writer;

use Afom\TranslationManager\Model\Translation;
use Afom\TranslationManager\Writer\Mapper\ArrayMapper;
use Symfony\Component\Filesystem\Filesystem;

abstract class TranslationWriter
{
    /** @var Filesystem */
    protected $filesystem;

    /** @var ArrayMapper */
    protected $arrayMapper;

    /** @var string */
    protected $destination;

    /**
     * @param Filesystem  $filesystem
     * @param ArrayMapper $arrayMapper
     * @param string      $destination
     */
    public function __construct(Filesystem $filesystem, ArrayMapper $arrayMapper, $destination)
    {
        $this->filesystem  = $filesystem;
        $this->arrayMapper = $arrayMapper;
        $this->destination  = $destination;
    }

    /**
     * @param Translation[] $translations
     *
     * @return array $ordered
     */
    protected function orderByLocale(array $translations)
    {
        $ordered = [];

        foreach ($translations as $translation) {
            $ordered[$translation->getLocale()][] = $translation;
        }

        return $ordered;
    }
}
