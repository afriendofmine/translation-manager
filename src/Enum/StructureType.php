<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Enum;

abstract class StructureType
{
    const FLATTENED = 'flattened';
    const NESTED    = 'nested';
}
