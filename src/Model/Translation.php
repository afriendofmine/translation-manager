<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Model;

class Translation
{
    /** @var string */
    private $identifier;

    /** @var string */
    private $locale;

    /** @var string */
    private $content;

    /** @var string */
    private $status = 'unknown';

    /**
     * @param string $identifier
     * @param string $locale
     * @param string $content
     */
    public function __construct($identifier, $locale, $content)
    {
        $this->identifier = strtolower($identifier);
        $this->locale     = $locale;
        $this->content    = $content;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
