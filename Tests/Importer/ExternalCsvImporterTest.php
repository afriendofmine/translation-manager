<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Tests\Importer;

use Afom\TranslationManager\Importer\ExternalCsvImporter;
use Afom\TranslationManager\Importer\Parser\CsvParser;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

class ExternalCsvImporterTest extends \PHPUnit_Framework_TestCase
{
    /** @var ExternalCsvImporter */
    private $externalCsvImporter;

    /** @var \PHPUnit_Framework_MockObject_MockObject|ClientInterface */
    private $client;

    /** @var \PHPUnit_Framework_MockObject_MockObject|CsvParser */
    private $csvParser;

    /** @var \PHPUnit_Framework_MockObject_MockObject|ResponseInterface */
    private $response;

    public function setUp()
    {
        $this->client    = $this->createClientInterfaceMock();
        $this->csvParser = $this->createCsvParserMock();
        $this->response  = $this->createResponseInterfaceMock();

        $this->externalCsvImporter = new ExternalCsvImporter($this->client, $this->csvParser, 'https://www.foo.bar/document.csv');
    }

    public function testImport()
    {
        $this->client->expects($this->once())
            ->method('request')
            ->with('GET', 'https://www.foo.bar/document.csv')
            ->willReturn($this->response);

        $this->response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);

        $this->response->expects($this->once())
            ->method('getHeader')
            ->with('Content-Type')
            ->willReturn(['text/csv']);

        $this->csvParser->expects($this->once())
            ->method('parse');

        $this->externalCsvImporter->import('https://www.foo.bar/document.csv');
    }

    /**
     * @expectedException \Afom\TranslationManager\Exception\ImporterException
     */
    public function testImportStatusCodeException()
    {
        $this->client->expects($this->once())
            ->method('request')
            ->with('GET', 'https://www.foo.bar/document.csv')
            ->willReturn($this->response);

        $this->response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(502);

        $this->response->expects($this->never())
            ->method('getHeader');

        $this->csvParser->expects($this->never())
            ->method('parse');

        $this->externalCsvImporter->import('https://www.foo.bar/document.csv');
    }

    /**
     * @expectedException \Afom\TranslationManager\Exception\ImporterException
     */
    public function testImportInvalidHeaderException()
    {
        $this->client->expects($this->once())
            ->method('request')
            ->with('GET', 'https://www.foo.bar/document.csv')
            ->willReturn($this->response);

        $this->response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);

        $this->response->expects($this->once())
            ->method('getHeader')
            ->with('Content-Type')
            ->willReturn(['html/txt']);

        $this->csvParser->expects($this->never())
            ->method('parse');

        $this->externalCsvImporter->import('https://www.foo.bar/document.csv');
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|ClientInterface
     */
    private function createClientInterfaceMock()
    {
        return $this->getMock(ClientInterface::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|CsvParser
     */
    private function createCsvParserMock()
    {
        return $this->getMockBuilder(CsvParser::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|ResponseInterface
     */
    private function createResponseInterfaceMock()
    {
        return $this->getMock(ResponseInterface::class);
    }
}
