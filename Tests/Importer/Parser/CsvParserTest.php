<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Tests\Importer\Parser;

use Afom\TranslationManager\Importer\Parser\CsvParser;
use Box\Spout\Reader\ReaderInterface;
use Symfony\Component\Filesystem\Filesystem;
use Box\Spout\Reader\CSV\Sheet;
use Box\Spout\Reader\CSV\RowIterator;

class CsvParserTest extends \PHPUnit_Framework_TestCase
{
    /** @var CsvParser */
    private $csvParser;

    /** @var \PHPUnit_Framework_MockObject_MockObject|ReaderInterface */
    private $reader;

    /** @var \PHPUnit_Framework_MockObject_MockObject|Filesystem */
    private $filesystem;

    /** @var \PHPUnit_Framework_MockObject_MockObject|Sheet */
    private $sheet;

    /** @var \PHPUnit_Framework_MockObject_MockObject|RowIterator */
    private $row;

    public function setUp()
    {
        $this->reader     = $this->createReaderInterfaceMock();
        $this->filesystem = $this->createFilesystemMock();
        $this->sheet      = $this->createSheetMock();
        $this->row        = $this->createRowIteratorMock();

        $this->csvParser = new CsvParser($this->reader, $this->filesystem);
    }

    public function testParse()
    {
        $this->filesystem->expects($this->once())
            ->method('exists')
            ->with('./mock/test.csv')
            ->willReturn(true);

        $this->reader->expects($this->once())
            ->method('open')
            ->with('./mock/test.csv');

        $this->reader->expects($this->once())
            ->method('getSheetIterator')
            ->willReturn([$this->sheet]);

        $this->sheet->expects($this->once())
            ->method('getRowIterator')
            ->willReturn([$this->row]);

        $this->assertTrue(is_array($this->csvParser->parse('./mock/test.csv')));
    }

    /**
     * @expectedException \Symfony\Component\Filesystem\Exception\FileNotFoundException
     */
    public function testParseFileNotFound()
    {
        $this->filesystem->expects($this->once())
            ->method('exists')
            ->with('./mock/test.csv')
            ->willReturn(false);

        $this->reader->expects($this->never())
            ->method('open');

        $this->csvParser->parse('./mock/test.csv');
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|ReaderInterface
     */
    private function createReaderInterfaceMock()
    {
        return $this->getMock(ReaderInterface::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|Filesystem
     */
    private function createFilesystemMock()
    {
        return $this->getMockBuilder(Filesystem::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|Sheet
     */
    private function createSheetMock()
    {
        return $this->getMockBuilder(Sheet::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|RowIterator
     */
    private function createRowIteratorMock()
    {
        return $this->getMockBuilder(RowIterator::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
