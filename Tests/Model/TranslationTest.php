<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Tests\Model;

use Afom\TranslationManager\Model\Translation;

class TranslationTest extends \PHPUnit_Framework_TestCase
{
    public function testTranslationModel()
    {
        $translation = new Translation('awesome-identifier', 'nl_nl', 'Awesome stuff!');

        $this->assertSame('awesome-identifier', $translation->getIdentifier());
        $this->assertSame('nl_nl', $translation->getLocale());
        $this->assertSame('Awesome stuff!', $translation->getContent());
        $this->assertSame('unknown', $translation->getStatus());

        $translation->setStatus('super-status');
        $this->assertSame('super-status', $translation->getStatus());


        $translation->setContent('Awesome content');
        $this->assertSame('Awesome content', $translation->getContent());
    }
}
