<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Tests\Writer;

use Afom\TranslationManager\Model\Translation;
use Afom\TranslationManager\Writer\JSONWriter;
use Afom\TranslationManager\Writer\Mapper\ArrayMapper;
use Symfony\Component\Filesystem\Filesystem;

class JSONWriterTest extends \PHPUnit_Framework_TestCase
{
    /** @var JSONWriter */
    private $jSONWriter;

    /** @var \PHPUnit_Framework_MockObject_MockObject|Filesystem */
    private $filesystem;

    /** @var \PHPUnit_Framework_MockObject_MockObject|ArrayMapper */
    private $arrayMapper;

    /** @var \PHPUnit_Framework_MockObject_MockObject|Translation */
    private $translation;

    public function setUp()
    {
        $this->filesystem  = $this->createFilesystemMock();
        $this->arrayMapper = $this->createArrayMapperMock();
        $this->translation = $this->createTranslationMock();

        $this->jSONWriter = new JSONWriter($this->filesystem, $this->arrayMapper, 'path-to-export');
    }

    public function testWrite()
    {
        $this->translation->expects($this->once())
            ->method('getLocale')
            ->willReturn('nl_nl');

        $this->arrayMapper->expects($this->once())
            ->method('toArray')
            ->with([$this->translation])
            ->willReturn(['sjenkie' => 'in de koelkast']);

        $this->filesystem->expects($this->once())
            ->method('dumpFile')
            ->with('path-to-export/nl_nl/awesome-file-name.json', '{"sjenkie":"in de koelkast"}');

        $this->jSONWriter->write([$this->translation], 'awesome-file-name');
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|Filesystem
     */
    private function createFilesystemMock()
    {
        return $this->getMockBuilder(Filesystem::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|ArrayMapper
     */
    private function createArrayMapperMock()
    {
        return $this->getMock(ArrayMapper::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|Translation
     */
    private function createTranslationMock()
    {
        return $this->getMockBuilder(Translation::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
