<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Tests\Writer\Mapper;

use Afom\TranslationManager\Model\Translation;
use Afom\TranslationManager\Writer\Mapper\ArrayMapper;
use Afom\TranslationManager\Enum\StructureType;

class ArrayMapperTest extends \PHPUnit_Framework_TestCase
{
    /** @var ArrayMapper */
    private $arrayMapper;

    /** @var \PHPUnit_Framework_MockObject_MockObject|Translation */
    private $translation;

    public function setUp()
    {
        $this->translation = $this->createTranslationMock();

        $this->arrayMapper = new ArrayMapper();
    }

    public function testToArray()
    {
        $this->translation->expects($this->exactly(2))
            ->method('getIdentifier')
            ->willReturn('awesome-key');

        $this->translation->expects($this->once())
            ->method('getContent')
            ->willReturn('Super content');

        $this->assertSame(['awesome-key' => 'Super content'], $this->arrayMapper->toArray([$this->translation]));
    }

    public function testToArrayNested()
    {
        $this->arrayMapper->setStructure(StructureType::NESTED);

        $this->translation->expects($this->once())
            ->method('getIdentifier')
            ->willReturn('sjenkie.in.de.koelkast');

        $this->translation->expects($this->once())
            ->method('getContent')
            ->willReturn('Super content');

        $this->assertSame([
            'sjenkie' => [
                'in' => [
                    'de' => [
                        'koelkast' => 'Super content',
                    ]
                ]
            ]
        ], $this->arrayMapper->toArray([$this->translation]));
    }

    public function testToArrayNestedWithArray()
    {
        $this->arrayMapper->setStructure(StructureType::NESTED);

        $this->translation->expects($this->once())
            ->method('getIdentifier')
            ->willReturn('sjenkie.in.de.koelkast.array');

        $this->translation->expects($this->once())
            ->method('getContent')
            ->willReturn('Super content');

        $this->assertSame([
            'sjenkie' => [
                'in' => [
                    'de' => [
                        'koelkast' => [
                            'array' => ['Super content']
                        ]
                    ]
                ]
            ]
        ], $this->arrayMapper->toArray([$this->translation]));
    }

    public function testToArrayNestedWithKeyIsNull()
    {
        $this->arrayMapper->setStructure(StructureType::NESTED);

        $this->translation->expects($this->once())
            ->method('getIdentifier')
            ->willReturn(null);

        $this->translation->expects($this->once())
            ->method('getContent')
            ->willReturn(['Super content']);

        $this->assertSame([], $this->arrayMapper->toArray([$this->translation]));
    }

    public function testToArrayFlattenedWithArray()
    {
        $this->arrayMapper->setStructure(StructureType::FLATTENED);

        $this->translation->expects($this->exactly(2))
            ->method('getIdentifier')
            ->willReturn('awesome.identifier.array');

        $this->translation->expects($this->once())
            ->method('getContent')
            ->willReturn('Super content');

        $this->assertSame(['awesome.identifier.array' => ['Super content']], $this->arrayMapper->toArray([$this->translation]));
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|Translation
     */
    private function createTranslationMock()
    {
        return $this->getMockBuilder(Translation::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
