<?php
/*
* (c) afriendofmine B.V. <development@afriendofmine.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Afom\TranslationManager\Tests\Enum;

use Afom\TranslationManager\Enum\StructureType;

class StructureTypeTest extends \PHPUnit_Framework_TestCase
{
    public function testEnum()
    {
        $this->assertSame('flattened', StructureType::FLATTENED);
        $this->assertSame('nested', StructureType::NESTED);
    }
}
