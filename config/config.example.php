<?php

return [
    /*
    |--------------------------------------------------------------------------
    | General configuration
    |--------------------------------------------------------------------------
    |
    */
    'general'   => [
        'filename'   => 'app',
    ],

    /*
    |--------------------------------------------------------------------------
    | Importers configuration
    |--------------------------------------------------------------------------
    |
    */
    'importers' => [

        'adapter' => 'external',

        'external' => [
            [
                'filename' => 'app',
                'source'   => 'https://google.docs/example',
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Writers configuration
    |--------------------------------------------------------------------------
    |
    */
    'writers'   => [
        'json' => [
            'structure'   => \Afom\TranslationManager\Enum\StructureType::FLATTENED,
            'destination' =>'/path/to/export',
        ],
        'php'  => [
            'structure'   => \Afom\TranslationManager\Enum\StructureType::NESTED,
            'destination' =>'/path/to/export',
        ],
    ],
];
